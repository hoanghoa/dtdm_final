import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image
} from 'react-native';
import moment from 'moment';



export default class App extends React.Component {


  constructor(props){
		super(props);
    this.state = {
      isLoading: true,
      current_temp: null,
      current_humi: null,
      after1h_temp: null,
      after12h_temp: null,
      after24h_temp: null,
      clientInputData: null
    }
	}


  componentWillMount() {
    console.disableYellowBox = true;
    this.getCurrentTime();
  }

  getCurrentTime = () => {
    let hour = new Date().getHours();
    let minutes = new Date().getMinutes();
    let seconds = new Date().getSeconds();
    let am_pm = 'pm';

    if (minutes < 10) {
      minutes = '0' + minutes;
    }

    if (seconds < 10) {
      seconds = '0' + seconds;
    }

    if (hour > 12) {
      hour = hour - 12;
    }

    if (hour == 0) {
      hour = 12;
    }

    if (new Date().getHours() < 12) {
      am_pm = 'am';
    }

    this.setState({ currentTime: hour + ':' + minutes +':'+ seconds +' '+ am_pm });

  }

  componentWillUnmount() {
    console.disableYellowBox = true;
    clearInterval(this.timer);
  }

  getCurrentData(){
    console.disableYellowBox = true;
    return fetch('http://dtdmnhom13.tk/iot')
          .then((response)=> response.json())
          .then((responseJson) =>{
            console.log(responseJson);
                this.setState({
                      isLoading: false,
                      current_temp: responseJson.current_temperature,
                      current_humi: responseJson.current_humidity,
                      after1h_temp: responseJson.next_temperature,
                  })

            })
          .catch((error) =>{
              console.log(error)
            });
  }

  getAfter12hData(){
    console.disableYellowBox = true;
    return fetch('http://dtdmnhom13.tk/iot/time/43200000')
          .then((response)=> response.json())
          .then((responseJson) =>{
            console.log(responseJson);
                this.setState({
                      isLoading: false,
                      after12h_temp: responseJson.next_temperature,
                  })

            })
          .catch((error) =>{
              console.log(error)
            });
  }

  getAfter24hData(){
    console.disableYellowBox = true;
    return fetch('http://dtdmnhom13.tk/iot/time/86400000')
          .then((response)=> response.json())
          .then((responseJson) =>{
            console.log(responseJson);
                this.setState({
                      isLoading: false,
                      after24h_temp: responseJson.next_temperature,
                  })

            })
          .catch((error) =>{
              console.log(error)
            });
  }

  getDataClient(){
    console.disableYellowBox = true;
    return fetch('http://dtdmnhom13.tk/iot/30.5')
          .then((response)=> response.json())
          .then((responseJson) =>{
            console.log(responseJson);
                this.setState({
                      isLoading: false,
                      clientInputData: responseJson,
                  })

            })
          .catch((error) =>{
              console.log(error)
            });
  }

  componentDidMount (){
    console.disableYellowBox = true;
    this.timer = setInterval(() => {
      this.getCurrentTime();
    }, 1000);
      this.getCurrentData();
      this.getAfter12hData();
      this.getAfter24hData();
      this.getDataClient();
  }

  render(){
    if(this.state.isLoading){
      return(
          <View style = {styles.container}>
            <Text>Weather App</Text>
          </View>
        )
    }
    else{
      let current_temp = this.state.current_temp;
      let current_humi = this.state.current_humi;
      let next1h_temp = this.state.after1h_temp;
      let next12h_temp = this.state.after12h_temp;
      let next24h_temp = this.state.after24h_temp;
      var currentDate = moment().add(1,'days').format("DD/MM/YYYY");
      return(
        <View style = {styles.container}>
            <View style = {styles.header}>
            <Image style = {styles.image} source={require('./image/cloud.png')} />
              <Text style = {styles.locate}>Hồ Chí Minh</Text>
            </View>
            <View style = {styles.content}>
                <View style = {{flexDirection: 'row',}}>
                  <Text style = {{fontSize: 25,}}>Nhiệt độ: </Text>
                  <Text style = {styles.temp}>{current_temp}°C</Text>
                </View>
                <View style = {{flexDirection: 'row', marginTop: 15,}}>
                  <Text style = {{fontSize: 22,}}>Độ ẩm: </Text>
                  <Text style = {styles.humi}>{current_humi}%</Text>
                </View>
            </View>
            <View style = {styles.footer}>
              <View View style={{borderRadius: 3, width: 120, height: 120, backgroundColor: 'powderblue'}}>
                  <Text  style = {styles.text}>After 60m</Text>
                  <Text style = {styles.temp1} >{next1h_temp}°C</Text>
              </View>
              <View style={{borderRadius: 3, width: 120, height: 120, backgroundColor: 'skyblue'}}>
                  <Text style = {styles.text}>After 12h</Text>
                  <Text style = {styles.temp1} >{next12h_temp}°C</Text>
              </View>
              <View style={{borderRadius: 3, width: 120, height: 120, backgroundColor: 'steelblue'}}>
                  <Text style = {styles.text}>After 24h</Text>
                  <Text style = {styles.temp1} >{next24h_temp}°C</Text>
              </View>
            </View>
        </View>
        )
    }

  }
}

const styles = StyleSheet.create({
      container:{
      flex: 1,
      backgroundColor:'#FBEFF8',
    },
      item:{
        flex: 1,
      },
      image:{
        width: 150,
        height: 150,
        marginLeft: 20,
      },
      figure:{
        flexDirection: 'row',
        marginLeft: 30,
        marginTop: 30,
        },
      temp:{
        fontSize: 25,
        alignItems: 'center',
      },
      temp1:{
        fontSize: 22,
        marginTop: 10,
        marginLeft: 35,
        alignItems: 'center',
      },
      text:{
        fontSize:25,
        marginTop: 10,
        marginLeft: 5,
      },
      humi:{
        fontSize: 22,
      },
      header:{
        flexDirection: 'row',
        marginVertical: 75,
        },
        locate:{
          fontSize: 30,
          fontWeight: 'bold',
          marginLeft: 10,
        },
      footer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-around',
        marginTop: 150,
      },
      content: {
        marginTop: 10,
        marginLeft: 20,
        flexDirection: 'column',
      },
  });
